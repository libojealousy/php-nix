<?php

class Polygon
{
    private $numOfSides, $area, $kind;

    public function set_numOfSides($numOfSides) {
        $this->numOfSides = $numOfSides;
        return $this;
    }
    public function set_area($area) {
        $this->area = $area;
        return $this;
    }
    public function set_kind($kind) {
        $this->kind = $kind;
        return $this;
    }

    public function get_numOfSides()
    {
        return $this->numOfSides;
    }
    public function get_area()
    {
        return $this->area;
    }
    public function get_kind()
    {
        return $this->kind;
    }

    public function get_info()
    {
        return  "The number of sides: " . $this->get_numOfSides() . ", area: " . $this->get_area() . ", kind: " . $this->get_kind();
    }
}

class Rectangle extends Polygon {
    private $width, $height, $perimeter;

    public function set_width($width) {
        $this->width = $width;
        return $this;
    }
    public function set_height($height) {
        $this->height = $height;
        return $this;
    }
    public function set_perimeter($perimeter) {
        $this->$perimeter = $perimeter;
        return $this;
    }
    public function get_width()
    {
        return $this->width;
    }
    public function get_height()
    {
        return $this->height;
    }
    public function get_perimeter()
    {
        return $this->perimeter;
    }
    public function get_info()
    {
        return parent::get_info() . ", width: " . $this->get_width() . ", height: " . $this->get_height() . ", perimeter: " . $this->get_perimeter();
    }
}
class Square extends Rectangle {
    private $sideSize, $diameter, $circleRadius;

    public function set_sideSize($sideSize) {
        $this->sideSize = $sideSize;
        return $this;
    }
    public function set_diameter($diameter) {
        $this->diametr = $diameter;
        return $this;
    }
    public function set_circleRadius($circleRadius) {
        $this->circleRadius = $circleRadius;
        return $this;
    }
    public function get_sideSize()
    {
        return $this->sideSize;
    }
    public function get_diameter()
    {
        return $this->diameter;
    }
    public function get_circleRadius()
    {
        return $this->circleRadius;
    }
    public function get_info()
    {
        return parent::get_info() . " size of side: " . $this->get_sideSize() . ", diameter: " . $this->get_diameter() . ", circle radius: " . $this->get_circleRadius();
    }

}

$quadrilateral = new Polygon();
$quadrilateral
    ->set_numOfSides("4")
    ->set_kind("впуклый")
    ->set_area("16");

$rectangle= new Rectangle();
$rectangle
    ->set_numOfSides("4")
    ->set_kind("впуклый")
    ->set_area("16")
    ->set_width("7")
    ->set_height("7")
    ->set_perimeter("28");

$square = new Square();
$square
    ->set_numOfSides("4")
    ->set_kind("впуклый")
    ->set_area("16")
    ->set_width("7")
    ->set_height("7")
    ->set_perimeter("28")
    ->set_sideSize("7")
    ->set_diameter("5")
    ->set_circleRadius("8");
echo $quadrilateral->get_info() ."<pre>";
echo $rectangle->get_info() ."<pre>";
echo $square->get_info() ."<pre>";
