<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                               <!-- CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                                                <!-- JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <title>TASK 8</title>
</head>
<body>
<div class="container">
    <form action="index.php" method="post">
        <div class="form-group col-md-4">
            <input type="text" class="form-control badge badge-pill badge-light" name="City" placeholder="Введите город" required>
        </div>
        <div class="form-group col-md-2">
            <input type="number" class="form-control" name="Index" placeholder="Введите индекс" required>
        </div>
        <div class="form-group col-md-12">
           <input type="text" class="form-control" name="Adress" placeholder="Введите адресс" required>
        </div>
        <div class="form-group col-md-6">
            <input type="email" class="form-control" name="Email" placeholder="Введите email" required>
        </div>
        <div class="form-group col-md-6">
            <input type="password" class="form-control" name="Password" placeholder="Введите пароль" required>
        </div>
        <br>
    <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" required> Проверка ввода
        </div>
    </div>
        <br>
        <button type="submit" class="btn btn-info btn-md">Отправить</button>
        <button type="reset" class="btn btn-outline-primary">Очистить</button>
    </form>
</div>
</body>
</html>

