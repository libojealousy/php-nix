<?php

class Books {

    protected $name, $author, $genre;

    public function __set($b, $c)
    {
        if (property_exists($this, $b)) {
            $this->$b = $c;
        }
    }
    public function getInformation($book)
    {
        $value = 1;
        foreach($book->query('SELECT * FROM books') as $row)
        {
            echo $value . '. ' . '"' . $row[1] . '"' .' (author: ' . $row[2] .
                ', genre: ' . $row[3] . ')<br>';
            $value++;
        }
    }

    public function addTo($con)
    {
        $con->query("INSERT INTO books(name, author, genre) 
                   VALUES('$this->name', '$this->author', '$this->genre')");
    }
}


class DB {

    static $obj;

    private function __clone(){}
    private function __wakeup(){}

    private function __construct(){
        $this->con = new mysqli("127.0.01", "root", "12345", "books");
    }

    static public function getConnect(){
        if(empty(self::$obj))
            self::$obj = new self();
        return self::$obj->con;
    }

}


$all_books = new Books();
$book = new Books();
$book->name = 'Банды четырех';
$book->author = 'Мэтт Зандстр, Александр Швец';
$book->genre = 'learning';
$book->addTo(DB::getConnect());

echo $all_books->getInformation(DB::getConnect());
