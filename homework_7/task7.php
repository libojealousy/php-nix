<?php
//1
echo '<pre>#1<pre>';
//function expression
$minCh=function ($a,$b,$c)
{
    if($a<$b && $a<$c){ echo 'min number:'.$a;}
    else if($b<$a && $b<$c){ echo 'min number:'.$b;}
    else if($c<$b && $c<$a){ echo 'min number:'.$c;}
};
$maxCh=function ($a,$b,$c)
{
    if($a>$b && $a>$c){ echo 'max number:'.$a;}
    else if($b>$a && $b>$c){ echo 'max number:'.$b;}
    else if($c>$b && $c>$a){ echo 'max number:'.$c;}
};
echo '<pre>';
echo $minCh(1,2,4);
echo '<pre>';
echo $maxCh(1,2,4);
//Стрелочная функция
function MinCh($a,$b,$c){
    if($a<$b && $a<$c){ echo 'min number:'.$a;}
    else if($b<$a && $b<$c){ echo 'min number:'.$b;}
    else if($c<$b && $c<$a){ echo 'min number:'.$c;}
}
$MinChi= fn ($a,$b,$c)=> MinCh($a,$b,$c);
echo '<pre>';
echo $MinChi(0,1,2);

function MaxCh($a,$b,$c){
    if($a>$b && $a>$c){ echo 'max number:'.$a;}
    else if($b>$a && $b>$c){ echo 'max number:'.$b;}
    else if($c>$b && $c>$a){ echo 'max number:'.$c;}
}
$MaxChi= fn ($a,$b,$c)=> MaxCh($a,$b,$c);
echo '<pre>';
echo $MaxChi(0,1,2);

//2
echo '<pre>#2<pre>';
//function expression
$square1=function ($a,$b){
    return $a*$b;
};
echo "square is ". $square1(11,3);
//Стрелочная функция
$square2=fn($a,$b)=>$a*$b;
echo '<pre>';
echo 'square is '.$square2(4,5);

//3
echo '<pre>#3<pre>';
//function expression
$Pifagor1=function ($a,$b){
    return 'c='.sqrt($a**2+$b**2);
};
echo $Pifagor1(5,12);
//Стрелочная функция
$Pifagor2=fn($a,$b)=>'c='.sqrt($a**2+$b**2);
echo '<pre>';
echo $Pifagor2(4,3);

//4
echo '<pre>#4<pre>';
//function expression
$perimetr=function ($a,$b,$c){
    return 'perimetr is '.($a+$b+$c);
};
echo $perimetr(3,5,8);
//Стрелочная функция
$perimetr2=fn($a,$b,$c)=>'perimetr is '.($a+$b+$c);
echo '<pre>';
echo $perimetr2(4,6,5);

//5
echo '<pre>#5<pre>';
//function expression
$discrim=function ($a,$b,$c){
    return 'D is '.($b**2-4*$a*$c);
};
echo $discrim(3,7,2);
//Стрелочная функция
$discrim2=fn($a,$b,$c)=>'D is '.($b**2-4*$a*$c);
echo '<pre>';
echo $discrim2(5,12,3);

//6
echo '<pre>#6<pre>';
//function expression
$numbersTo=function ()
{
    for ($i=0;$i<100;$i+=2){
        echo $i.'<br>';
    }
    return $i;
};
$numbersTo();
//Стрелочная функция
$numbersTo2=fn()=>$numbersTo();
echo '<pre>';
$numbersTo2();

//7
echo '<pre>#7<pre>';
//function expression
$numbersTo3=function ()
{
    for ($i=1;$i<100;$i+=2){
        echo $i.'<br>';
    }
    return $i;
};
$numbersTo3();
//Стрелочная функция
$numbersTo4=fn()=>$numbersTo3();
echo '<pre>';
$numbersTo4();

//8
echo '<pre>#8<pre>';
//function expression
$arr=rand(0,10);
$elements=function ($a){
    $povt=0;
    for ($i=0;$i<10;$i++){
        if ($a[$i]==$a[$i+1]){$povt++;}
    }
    if ($povt>0){return 'it is';}
    else{ return 'no';}
};
echo $elements($arr);
//Стрелочная функция
$elements2=fn($a)=>$elements($a);
echo '<pre>';
echo $elements2($arr);

//9
echo '<pre>#9<pre>';
//function expression
$sort = function ($arr, $sort = 'adc')
{
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    if ($sort == 'adc') {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }

    }
    else {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] < $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }

    }
    return $arr;
};
echo '<pre>';
print_r($sort([9,15,4,2],'adc'));
//Стрелочная функция
function sortUp($arr)
{
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
    }

    return $arr;
}

function sortDown($arr)
{
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }

        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] < $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }

    return $arr;
}

$sort2 = fn ($arr, $sort = 'adc') => $sort == 'adc' ? sortUp($arr) : sortDown($arr);
echo '<pre>';
print_r($sort2([5,4,6,8]));

  ?>


