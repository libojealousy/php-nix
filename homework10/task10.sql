mysql> create database films;
mysql> use films;

mysql> create table actors(
    -> id int not null primary key auto_increment,
    -> name varchar(20) not null
    -> , surname varchar(30) not null,
    -> age int(100) not null
    -> );

mysql> desc actors;
+---------+-------------+------+-----+---------+----------------+
| Field   | Type        | Null | Key | Default | Extra          |
+---------+-------------+------+-----+---------+----------------+
| id      | int(11)     | NO   | PRI | NULL    | auto_increment |
| name    | varchar(20) | NO   |     | NULL    |                |
| surname | varchar(30) | NO   |     | NULL    |                |
| age     | int(100)    | NO   |     | NULL    |                |
+---------+-------------+------+-----+---------+----------------+

mysql> create table filmes(
    -> id int not null primary key auto_increment,
    -> title varchar(20) not null,
    -> year_of_issue int(4) not null,
    -> rating int(10) not null
    -> );

mysql> desc filmes;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| id            | int(11)     | NO   | PRI | NULL    | auto_increment |
| title         | varchar(20) | NO   |     | NULL    |                |
| year_of_issue | int(4)      | NO   |     | NULL    |                |
| rating        | int(10)     | NO   |     | NULL    |                |
+---------------+-------------+------+-----+---------+----------------+

mysql> create table directors(
    -> id int not null primary key auto_increment,
    -> name varchar(20) not null,
    -> surname varchar(30) not null,
    -> age int(100) not null
    -> );

mysql> desc directors;
+---------+-------------+------+-----+---------+----------------+
| Field   | Type        | Null | Key | Default | Extra          |
+---------+-------------+------+-----+---------+----------------+
| id      | int(11)     | NO   | PRI | NULL    | auto_increment |
| name    | varchar(20) | NO   |     | NULL    |                |
| surname | varchar(30) | NO   |     | NULL    |                |
| age     | int(100)    | NO   |     | NULL    |                |
+---------+-------------+------+-----+---------+----------------+

mysql> create table new_actors as SELECT * FROM actors;

mysql> desc new_actors;
+---------+-------------+------+-----+---------+-------+
| Field   | Type        | Null | Key | Default | Extra |
+---------+-------------+------+-----+---------+-------+
| id      | int(11)     | NO   |     | 0       |       |
| name    | varchar(20) | NO   |     | NULL    |       |
| surname | varchar(30) | NO   |     | NULL    |       |
| age     | int(100)    | NO   |     | NULL    |       |
+---------+-------------+------+-----+---------+-------+

mysql> drop table new_actors;
mysql> desc new_actors;
ERROR 1146 (42S02): Table 'films.new_actors' doesn't exist

mysql> alter table directors add column number_of_films int not null;

mysql> desc directors;
+-----------------+-------------+------+-----+---------+----------------+
| Field           | Type        | Null | Key | Default | Extra          |
+-----------------+-------------+------+-----+---------+----------------+
| id              | int(11)     | NO   | PRI | NULL    | auto_increment |
| name            | varchar(20) | NO   |     | NULL    |                |
| surname         | varchar(30) | NO   |     | NULL    |                |
| age             | int(100)    | NO   |     | NULL    |                |
| number_of_films | int(11)     | NO   |     | NULL    |                |
+-----------------+-------------+------+-----+---------+----------------+

mysql> INSERT INTO directors (name, surname, age, number_of_films) VALUES
    -> ("Piter", "Jackson", "46", "6"),
    -> ("James", "Cameron", "42", "7"),
    -> ("Frank", "Darabont", "64", "12"),
    -> ("Steiven", "Spilberg", "82", "76");


mysql> INSERT INTO actors (name, surname, age) VALUES
    -> ("Alaydzha", "Wood", "32"),
    -> ("Tom", "Hancks", "53"),
    -> ("Leonardo", "Dickaprio", "56"),
    -> ("Jhoney", "Depp", "54");

mysql> INSERT INTO filmes (title, year_of_issue, rating) VALUES
    -> ("Lord of the Rings 3", "2003", "9"),
    -> ("Terminator 2", "1991", "9"),
    -> ("Forrest Gump", "1994", "9");
Query OK, 3 rows affected (0.08 sec)
Records: 3  Duplicates: 0  Warnings: 0

mysql> SELECT * FROM filmes;
+----+---------------------+---------------+--------+
| id | title               | year_of_issue | rating |
+----+---------------------+---------------+--------+
|  1 | Lord of the Rings 3 |          2003 |      9 |
|  2 | Terminator 2        |          1991 |      9 |
|  3 | Forrest Gump        |          1994 |      9 |
+----+---------------------+---------------+--------+
3 rows in set (0.00 sec)

mysql> update filmes set id=4 where id=1;

mysql> SELECT * FROM filmes;
+----+---------------------+---------------+--------+
| id | title               | year_of_issue | rating |
+----+---------------------+---------------+--------+
|  2 | Terminator 2        |          1991 |      9 |
|  3 | Forrest Gump        |          1994 |      9 |
|  4 | Lord of the Rings 3 |          2003 |      9 |
+----+---------------------+---------------+--------+

