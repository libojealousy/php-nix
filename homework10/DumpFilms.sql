-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               8.0.11 - MySQL Community Server - GPL
-- Операционная система:         Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дамп структуры базы данных films
CREATE DATABASE IF NOT EXISTS `films` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `films`;

-- Дамп структуры для таблица films.actors
CREATE TABLE IF NOT EXISTS `actors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `age` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы films.actors: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` (`id`, `name`, `surname`, `age`) VALUES
	(1, 'Alaydzha', 'Wood', 32),
	(2, 'Tom', 'Hancks', 53),
	(3, 'Leonardo', 'Dickaprio', 56),
	(4, 'Jhoney', 'Depp', 54);
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;

-- Дамп структуры для таблица films.directors
CREATE TABLE IF NOT EXISTS `directors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `age` int(100) NOT NULL,
  `number_of_films` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы films.directors: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `directors` DISABLE KEYS */;
INSERT INTO `directors` (`id`, `name`, `surname`, `age`, `number_of_films`) VALUES
	(1, 'Piter', 'Jackson', 46, 6),
	(2, 'James', 'Cameron', 42, 7),
	(3, 'Frank', 'Darabont', 64, 12),
	(4, 'Steiven', 'Spilberg', 82, 76);
/*!40000 ALTER TABLE `directors` ENABLE KEYS */;

-- Дамп структуры для таблица films.filmes
CREATE TABLE IF NOT EXISTS `filmes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `year_of_issue` int(4) NOT NULL,
  `rating` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы films.filmes: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `filmes` DISABLE KEYS */;
INSERT INTO `filmes` (`id`, `title`, `year_of_issue`, `rating`) VALUES
	(2, 'Terminator 2', 1991, 9),
	(3, 'Forrest Gump', 1994, 9),
	(4, 'Lord of the Rings 3', 2003, 9);
/*!40000 ALTER TABLE `filmes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
