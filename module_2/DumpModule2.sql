-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               8.0.11 - MySQL Community Server - GPL
-- Операционная система:         Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дамп структуры базы данных module2
CREATE DATABASE IF NOT EXISTS `module2` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `module2`;

-- Дамп структуры для таблица module2.task1
CREATE TABLE IF NOT EXISTS `task1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(22) NOT NULL,
  `pwd` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gender` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы module2.task1: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `task1` DISABLE KEYS */;
INSERT INTO `task1` (`id`, `name`, `pwd`, `email`, `gender`) VALUES
	(1, 'Vasya', '21341234qwfsdf', 'mmm@mmail.com', 'm'),
	(2, 'Alex', '21341234', 'mmm@gmail.com', 'm'),
	(3, 'Alexey', 'qq21341234Q', 'alexey@gmail.com', 'm'),
	(4, 'Helen', 'MarryMeeee', 'hell@gmail.com', 'f'),
	(5, 'Jenny', 'SmakeMyb', 'eachup@gmail.com', 'f'),
	(6, 'Lora', 'burn23', 'tpicks@gmail.com', 'f');
/*!40000 ALTER TABLE `task1` ENABLE KEYS */;

-- Дамп структуры для таблица module2.vocabulary
CREATE TABLE IF NOT EXISTS `vocabulary` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `info` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы module2.vocabulary: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `vocabulary` DISABLE KEYS */;
INSERT INTO `vocabulary` (`id`, `name`, `info`) VALUES
	(1, 'animals', NULL),
	(2, 'school', NULL),
	(3, 'nature', NULL),
	(4, 'human', NULL),
	(5, 'SF', NULL);
/*!40000 ALTER TABLE `vocabulary` ENABLE KEYS */;

-- Дамп структуры для таблица module2.word
CREATE TABLE IF NOT EXISTS `word` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) DEFAULT NULL,
  `vocabulary_id` int(11) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы module2.word: ~50 rows (приблизительно)
/*!40000 ALTER TABLE `word` DISABLE KEYS */;
INSERT INTO `word` (`id`, `word`, `vocabulary_id`) VALUES
	(1, 'turtle', 1),
	(2, 'pig', 1),
	(3, 'dog', 1),
	(4, 'cat', 1),
	(5, 'lizard', 1),
	(6, 'cow', 1),
	(7, 'rabbit', 1),
	(8, 'frog', 1),
	(9, 'headgehog', 1),
	(10, 'goat', 1),
	(11, 'desk', 2),
	(12, 'book', 2),
	(13, 'chalk', 2),
	(14, 'pen', 2),
	(15, 'pencil', 2),
	(16, 'copybook', 2),
	(17, 'lesson', 2),
	(18, 'teacher', 2),
	(19, 'pupils', 2),
	(20, 'school', 2),
	(21, 'ray', 3),
	(22, 'thunder', 3),
	(23, 'sun', 3),
	(24, 'field', 3),
	(25, 'hill', 3),
	(26, 'mountain', 3),
	(27, 'river', 3),
	(28, 'forest', 3),
	(29, 'grass', 3),
	(30, 'rain', 3),
	(31, 'hair', 4),
	(32, 'nail', 4),
	(33, 'finger', 4),
	(34, 'eye', 4),
	(35, 'tooth', 4),
	(36, 'knee', 4),
	(37, 'elbow', 4),
	(38, 'leg', 4),
	(39, 'arm', 4),
	(40, 'head', 4),
	(41, 'engine', 5),
	(42, 'steel', 5),
	(43, 'power', 5),
	(44, 'nuclear', 5),
	(45, 'shotgun', 5),
	(46, 'laser', 5),
	(47, 'flight', 5),
	(48, 'energy', 5),
	(49, 'Moon', 5),
	(50, 'splace', 5);
/*!40000 ALTER TABLE `word` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
