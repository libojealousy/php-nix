mysql> create database module2;
mysql> use module2;

//1
mysql> create table task1(
    -> id int primary key auto_increment,
    -> name varchar(22) not null,
    -> pwd varchar(30) not null,
    -> email varchar(50) not null,
    -> gender varchar(1) not null
    -> );
mysql> INSERT INTO task1 (name, pwd, email, gender) VALUES
    -> ("Vasya", "21341234qwfsdf", "mmm@mmail.com", "m" ),
    -> ("Alex", "21341234", "mmm@gmail.com", "m" ),
    -> ("Alexey", "qq21341234Q", "alexey@gmail.com", "m" ),
    -> ("Helen", "MarryMeeee", "hell@gmail.com", "f" ),
    -> ("Jenny", "SmakeMyb", "eachup@gmail.com", "f" ),
    -> ("Lora", "burn23", "tpicks@gmail.com", "f" );

mysql> SELECT * FROM task1;
+----+--------+----------------+------------------+--------+
| id | name   | pwd            | email            | gender |
+----+--------+----------------+------------------+--------+
|  1 | Vasya  | 21341234qwfsdf | mmm@mmail.com    | m      |
|  2 | Alex   | 21341234       | mmm@gmail.com    | m      |
|  3 | Alexey | qq21341234Q    | alexey@gmail.com | m      |
|  4 | Helen  | MarryMeeee     | hell@gmail.com   | f      |
|  5 | Jenny  | SmakeMyb       | eachup@gmail.com | f      |
|  6 | Lora   | burn23         | tpicks@gmail.com | f      |
+----+--------+----------------+------------------+--------+

//2
mysql> SELECT CONCAT("This is ", name, ", ", IF(gender = "m", "he", "she"), " has email ", email) AS info FROM task1;
+-----------------------------------------------+
| info                                          |
+-----------------------------------------------+
| This is Vasya, he has email mmm@mmail.com     |
| This is Alex, he has email mmm@gmail.com      |
| This is Alexey, he has email alexey@gmail.com |
| This is Helen, she has email hell@gmail.com   |
| This is Jenny, she has email eachup@gmail.com |
| This is Lora, she has email tpicks@gmail.com  |
+-----------------------------------------------+

//3
mysql> SELECT CONCAT("We have ", COUNT(gender), IF(gender = "m", " boys!", " girls!")) AS "Gender information:" FROM task1 GROUP BY gender;
+---------------------+
| Gender information: |
+---------------------+
| We have 3 boys!     |
| We have 3 girls!    |
+---------------------+

//4
mysql> SELECT * FROM word;
+----+-----------+---------------+
| id | word      | vocabulary_id |
+----+-----------+---------------+
|  1 | turtle    |             1 |
|  2 | pig       |             1 |
|  3 | dog       |             1 |
|  4 | cat       |             1 |
|  5 | lizard    |             1 |
|  6 | cow       |             1 |
|  7 | rabbit    |             1 |
|  8 | frog      |             1 |
|  9 | headgehog |             1 |
| 10 | goat      |             1 |
| 11 | desk      |             2 |
| 12 | book      |             2 |
| 13 | chalk     |             2 |
| 14 | pen       |             2 |
| 15 | pencil    |             2 |
| 16 | copybook  |             2 |
| 17 | lesson    |             2 |
| 18 | teacher   |             2 |
| 19 | pupils    |             2 |
| 20 | school    |             2 |
| 21 | ray       |             3 |
| 22 | thunder   |             3 |
| 23 | sun       |             3 |
| 24 | field     |             3 |
| 25 | hill      |             3 |
| 26 | mountain  |             3 |
| 27 | river     |             3 |
| 28 | forest    |             3 |
| 29 | grass     |             3 |
| 30 | rain      |             3 |
| 31 | hair      |             4 |
| 32 | nail      |             4 |
| 33 | finger    |             4 |
| 34 | eye       |             4 |
| 35 | tooth     |             4 |
| 36 | knee      |             4 |
| 37 | elbow     |             4 |
| 38 | leg       |             4 |
| 39 | arm       |             4 |
| 40 | head      |             4 |
| 41 | engine    |             5 |
| 42 | steel     |             5 |
| 43 | power     |             5 |
| 44 | nuclear   |             5 |
| 45 | shotgun   |             5 |
| 46 | laser     |             5 |
| 47 | flight    |             5 |
| 48 | energy    |             5 |
| 49 | Moon      |             5 |
| 50 | splace    |             5 |
+----+-----------+---------------+

mysql> SELECT * FROM vocabulary;
+----+---------+------+
| id | name    | info |
+----+---------+------+
|  1 | animals | NULL |
|  2 | school  | NULL |
|  3 | nature  | NULL |
|  4 | human   | NULL |
|  5 | SF      | NULL |
+----+---------+------+

//5
mysql> SELECT name, COUNT(vocabulary_id) AS words FROM vocabulary, word WHERE vocabulary.id=word.vocabulary_id GROUP BY word.vocabulary_id;
+---------+-------+
| name    | words |
+---------+-------+
| animals |    10 |
| school  |    10 |
| nature  |    10 |
| human   |    10 |
| SF      |    10 |
+---------+-------+

























