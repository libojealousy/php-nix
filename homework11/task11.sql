CREATE table task11.actors LIKE films.actors;
ERROR 1049 (42000): Unknown database 'task11'
mysql> create database task11;
Query OK, 1 row affected (0.09 sec)

mysql> CREATE table task11.actors LIKE films.actors;

mysql> INSERT INTO task11.actors SELECT * FROM films.actors;

mysql> SELECT * FROM task11.actors;
+----+----------+-----------+-----+
| id | name     | surname   | age |
+----+----------+-----------+-----+
|  1 | Alaydzha | Wood      |  32 |
|  2 | Tom      | Hancks    |  53 |
|  3 | Leonardo | Dickaprio |  56 |
|  4 | Jhoney   | Depp      |  54 |
+----+----------+-----------+-----+

mysql> INSERT INTO task11.actors SET name ="Anjelina", surname ="Djolie", age=48;

mysql> SELECT * FROM task11.actors;
+----+----------+-----------+-----+
| id | name     | surname   | age |
+----+----------+-----------+-----+
|  1 | Alaydzha | Wood      |  32 |
|  2 | Tom      | Hancks    |  53 |
|  3 | Leonardo | Dickaprio |  56 |
|  4 | Jhoney   | Depp      |  54 |
|  5 | Anjelina | Djolie    |  48 |
+----+----------+-----------+-----+

mysql> SELECT * FROM task11.actors where age > 50;
+----+----------+-----------+-----+
| id | name     | surname   | age |
+----+----------+-----------+-----+
|  2 | Tom      | Hancks    |  53 |
|  3 | Leonardo | Dickaprio |  56 |
|  4 | Jhoney   | Depp      |  54 |
+----+----------+-----------+-----+

mysql> SELECT name, count(*) FROM task11.actors GROUP BY name;
+----------+----------+
| name     | count(*) |
+----------+----------+
| Alaydzha |        1 |
| Tom      |        1 |
| Leonardo |        1 |
| Jhoney   |        1 |
| Anjelina |        1 |
+----------+----------+

mysql> SELECT id, count(*) FROM task11.actors GROUP BY name;
+----+----------+
| id | count(*) |
+----+----------+
|  1 |        1 |
|  2 |        1 |
|  3 |        1 |
|  4 |        1 |
|  5 |        1 |
+----+----------+

mysql> SELECT name, count(*) FROM task11.actors GROUP BY id;
+----------+----------+
| name     | count(*) |
+----------+----------+
| Alaydzha |        1 |
| Tom      |        1 |
| Leonardo |        1 |
| Jhoney   |        1 |
| Anjelina |        1 |
+----------+----------+

mysql> SELECT id, count(*) FROM task11.actors GROUP BY id;
+----+----------+
| id | count(*) |
+----+----------+
|  1 |        1 |
|  2 |        1 |
|  3 |        1 |
|  4 |        1 |
|  5 |        1 |
+----+----------+

mysql> SELECT id, count(*) FROM task11.actors GROUP BY id;
+----+----------+
| id | count(*) |
+----+----------+
|  1 |        1 |
|  2 |        1 |
|  3 |        1 |
|  4 |        1 |
|  5 |        1 |
+----+----------+

mysql> SELECT name, age, id FROM task11.actors GROUP BY name, id;
+----------+-----+----+
| name     | age | id |
+----------+-----+----+
| Alaydzha |  32 |  1 |
| Tom      |  53 |  2 |
| Leonardo |  56 |  3 |
| Jhoney   |  54 |  4 |
| Anjelina |  48 |  5 |
+----------+-----+----+

mysql> SELECT name, max(age), id FROM task11.actors GROUP BY name, id;
+----------+----------+----+
| name     | max(age) | id |
+----------+----------+----+
| Alaydzha |       32 |  1 |
| Tom      |       53 |  2 |
| Leonardo |       56 |  3 |
| Jhoney   |       54 |  4 |
| Anjelina |       48 |  5 |
+----------+----------+----+

mysql> SELECT name, surname FROM task11.actors WHERE surname<6 order by age;
+----------+-----------+
| name     | surname   |
+----------+-----------+
| Alaydzha | Wood      |
| Anjelina | Djolie    |
| Tom      | Hancks    |
| Jhoney   | Depp      |
| Leonardo | Dickaprio |
+----------+-----------+

mysql> SELECT name, surname FROM task11.actors WHERE id<3 order by id;
+----------+---------+
| name     | surname |
+----------+---------+
| Alaydzha | Wood    |
| Tom      | Hancks  |
+----------+---------+

mysql> SELECT name, surname FROM task11.actors WHERE id<3 order by 1,2 desc;
+----------+---------+
| name     | surname |
+----------+---------+
| Alaydzha | Wood    |
| Tom      | Hancks  |
+----------+---------+

mysql> SELECT name, surname FROM task11.actors WHERE id<3 order by 1,2 desc limit 1;
+----------+---------+
| name     | surname |
+----------+---------+
| Alaydzha | Wood    |
+----------+---------+
