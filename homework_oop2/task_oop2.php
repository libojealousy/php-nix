<?php
/*
Создать несколько объектов существующих классов, реализовать
полиморфизм так, как вы его понимаете.
Разработать структуру классов для одного из
перечисленных вариантов:
млекопитающие, животные, человекообразные, человек
*/

interface canLive{ //L - Liskov substitution principle
    function live();
}

abstract class LiveIn implements canLive { //S - Single responsibility principle, O - Open/Closed principle, D - Dependency Inversion principle
    function live()
    {
        echo 'I live in ' . strtolower(get_class($this));
        echo '<br>' . $this->placeInfo() . '<br>';
    }
    abstract protected function placeInfo();
}

class AtLarge extends LiveIn {
    protected function placeInfo()
    {
        return '';
    }
}
class Home  extends LiveIn {
    protected function placeInfo()
    {
        return '';
    }
}


class Mlekopit {

    protected $kind;

    public function set_kind($kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getInfo()
    {
        return ('<br>' . $this->kind);
    }
}

class Animals extends Mlekopit {

    protected $levelOfEvolution, $kingdom;

    public function set_levelOfEvolution($levelOfEvolution)
    {
        $this->levelOfEvolution = $levelOfEvolution;
        return $this;
    }
    public function get_levelOfEvolution()
    {
        return $this->levelOfEvolution;
    }

    public function set_kingdom($kingdom)
    {
        $this->kingdom = $kingdom;
        return $this;
    }
    public function get_kingdom()
    {
        return $this->kingdom;
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>Level of evolution: ' . $this->get_levelOfEvolution() . ' , Animal kingdom: ' . $this->get_kingdom();
    }
}

class  Humanoobr extends Animals {

    protected $name, $rasa, $timeOfLife;

    public function set_name($name)
    {
        $this->name = $name;
        return $this;
    }
    public function get_name()
    {
        return $this->name;
    }

    public function set_rasa($rasa)
    {
        $this->rasa = $rasa;
        return $this;
    }
    public function get_rasa()
    {
        return $this->rasa;
    }

    public function set_timeOfLife($timeOfLife)
    {
        $this->timeOfLife = $timeOfLife;
        return $this;
    }
    public function get_timeOfLife()
    {
        return $this->timeOfLife;
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>' . $this->get_name() .
            ' , ' . $this->get_rasa() .
            ' , ' . $this->get_timeOfLife();
    }

}
class  Human extends Humanoobr {

    protected $initials, $age, $gender;
    private $live;

    public function set_initials($initials)
    {
        $this->initials = $initials;
        return $this;
    }
    public function get_initials()
    {
        return $this->initials;
    }

    public function set_age($age)
    {
        $this->age = $age;
        return $this;
    }
    public function get_age()
    {
        return $this->age;
    }

    public function set_gender($gender)
    {
        $this->gender = $gender;
        return $this;
    }
    public function get_gender()
    {
        return $this->gender;
    }

    public function selectPlace($place)
    {
        !is_object($place) ? : $this->live = $place;
    }
    public function live()
    {
        method_exists($this->live, 'live') &&
        is_subclass_of($this->live, "LiveIn") ?
            $this->live->live() : "I don't live";
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>' . $this->get_initials() .
            ' , ' . $this->get_gender() .
            ' , ' . $this->get_age();
    }

}

$flat = new Home();
$ann= new Human();
$ann
    ->set_kind('Placental mammals')
    ->set_kingdom('Primates')
    ->set_levelOfEvolution('high')
    ->set_name('human')
    ->set_rasa('Hominids')
    ->set_timeOfLife('3761 г. до н. э.')
    ->set_initials('A.S.O.')
    ->set_age('56')
    ->set_gender('female');
echo  $ann->getInfo() . '<hr>';
$ann->selectPlace($flat);
$ann->live();

