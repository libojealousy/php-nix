<?php
//1
while($a<=1000){
 $b=$a;
 $c=0;
 while($b>=1){
     if($a%$b==0)
         $c++;
     $b--;
 }
 echo $c==2? $a.'':'';
 $a++;
}
//2
$a=1;
$c=0;
while ($a<=100){
    if ($a%2==0){
        $c++;
    }
    $a++;
}
echo '<pre>'. $c;
//3
$c1 = 0;
$c2 = 0;
$c3 = 0;
$c4 = 0;
$c5 = 0;
for ($i = 1; $i <= 100; $i++) {
    $a = rand(1, 5);
    if ($a == 1) {
        $c1++;
    } elseif ($a == 2) {
        $c2++;
    } elseif ($a == 3) {
        $c3++;
    } elseif ($a == 4) {
        $c4++;
    } else {
        $c5++;
    }
}
echo '<br>'."1 - $c1" . '<br>' . "2 - $c2" . '<br>' . "3 - $c3" . '<br>' . "4 - $c4" . '<br>' . "5 - $c5";

//4
echo "<table>";
for ($i = 0; $i < 3; $i++) {
    echo "<tr>";
    for ($j = 0; $j < 5; $j++) {
        $c1 = rand(1, 256);
        $c2 = rand(1, 256);
        $c3 = rand(1, 256);
        echo "<td style=background-color:rgb($c1,$c2,$c3);>table</td>";
    }
    echo "</tr>";
}
echo "</table>";


//1**
$month=rand(1,12);
if($month<3 || $month==12){
    echo $month .' month-it is winter';
}
elseif($month>=3 && $month<6){
    echo $month .' month-it is spring';
}
elseif($month>=6 && $month<9){
    echo $month .' month-it is summer';
}
elseif($month>=9 && $month<12){
    echo $month .' month-it is autumn';
}

//2**
$string='abcde';
if($string[0]=='a'){
    echo '<pre>yes';
}
else{ echo '<pre>no';}

//3**
$string='12345';
if($string[0]=='1' || $string[0]=='2' || $string[0]=='3'){
    echo '<pre>yes';
}
else{ echo '<pre>no';}

//4**
$test=false;
//if else
if ($test==true){
    echo '<pre>correct';
}
else{echo '<pre>error';}

//tenarka
echo  ($test=false)? '<pre>correct':'<pre>error';

//5**
$lang='en';
//if else
echo '<pre>';
if ($lang=='ru'){
    print_r( ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']);
}
else{print_r( ['m','t','w','th','f','sat','s']);}
//tenarka
print_r($lang=='en' ? ['m','t','w','th','f','sat','s']:['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']);

//6**
$clock=rand(0,59);
//if else
if($clock>=0 && $clock<15){
    echo $clock .' minutes-first part';
}
elseif($clock>=15 && $clock<30){
    echo $clock .' minutes-second part';
}
elseif($clock>=30 && $clock<45){
    echo $clock .' minutes-third part';
}
elseif($clock>=45 && $clock<=59){
    echo $clock .' minutes-fourth part';
}
echo '<pre>';
//tenarka
echo(0<=$clock && $clock<15 ? "$clock min-first part":
    (15<=$clock && $clock<30 ? "$clock min-second part":
        (30<=$clock && $clock<45 ? "$clock min-third part":
            (45<=$clock && $clock<=59 ?"$clock min-fourth part": 'error'))));
echo '<pre>';


//1***
//for
$names=['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
for($i=4; $i>=0; $i--){
    echo $names[$i].' ';
}
echo '<pre>';

//while
$i = 0;
$index = 0;
$reverse = [];
while (isset($names[$i])) {
    $index = $i;
    $i++;
}
while ($index >= 0) {
    $reverse [] = $names[$index];
    $index--;
}
print_r($reverse);
echo '<pre>';

//do while
$i = 0;
$reverse = [];
while (isset($names[$i])) {
    $index = $i;
    $i++;
}
do {
    $reverse [] = $names[$index];
    $index--;
} while ($index >= 0);
print_r($reverse);
echo '<pre>';

//foreach
$index = 0;
$reverse = [];
foreach ($names as $key => $value) {
    if (isset($names[$key])) {
        $index = $key;
    }
}
foreach ($names as $key => $value) {
    $reverse[] = $names[$index];
    $index--;
}
print_r($reverse);
echo '<pre>';

//2***
//for
$arr=[44, 12, 11, 7, 1, 99, 43, 5, 69];
for($i=8; $i>=0; $i--){
    echo $arr[$i].' ';
}
echo '<pre>';

//while
$i = 0;
$index = 0;
$reverse = [];
while (isset($arr[$i])) {
    $index = $i;
    $i++;
}
while ($index >= 0) {
    $reverse [] = $arr[$index];
    $index--;
}
print_r($reverse);
echo '<pre>';

//do while
$i = 0;
$reverse = [];
while (isset($arr[$i])) {
    $index = $i;
    $i++;
}
do {
    $reverse [] = $arr[$index];
    $index--;
} while ($index >= 0);
print_r($reverse);
echo '<pre>';

//foreach
$index = 0;
$reverse = [];
foreach ($arr as $key => $value) {
    if (isset($arr[$key])) {
        $index = $key;
    }
}
foreach ($arr as $key => $value) {
    $reverse[] = $arr[$index];
    $index--;
}
print_r($reverse);
echo '<pre>';

//3***
//for
$str = 'Hi I am ALex';
for($i=11; $i>=0; $i--){
    echo $str[$i].' ';
}
echo '<pre>';

//while
$i = 0;
$index = 0;
$reverse = '';
while (isset($str[$i])) {
    $index = $i;
    $i++;
}
while ($index >= 0) {
    $reverse .= $str[$index];
    $index--;
}
print_r($reverse);
echo '<pre>';

//do while
$i = 0;
$reverse = '';
while (isset($str[$i])) {
    $index = $i;
    $i++;
}
do {
    $reverse .= $str[$index];
    $index--;
}
while ($index >= 0);
print_r($reverse);
echo '<pre>';

//foreach
$i = 0;
$index = 0;
$arr = [];
$reverseArr = [];
$reverse = '';
while (isset($str[$i])) {
    $arr[] = $str[$i];
    $i++;
}
foreach ($arr as $key => $value) {
    if (isset($arr[$key])) {
        $i = $key;
    }
}
foreach ($arr as $key => $value) {
    $reverseArr[] = $arr[$i];
    $index = $i;
    $i--;
}
foreach ($reverseArr as $value) {
    $reverse .= $reverseArr[$index];
    $index++;
}
print_r($reverse);
echo '<pre>';

//4***
//for
echo '<pre>';
$str = 'Hi I am ALex';
for($i=0; $i<12; $i++){
    echo mb_strtolower($str[$i]).' ';
}
echo '<pre>';

//while
$i = 0;
$lower = '';
while (isset($str[$i])) {
    $lower .= mb_strtolower($str[$i]);
    $i++;
}
echo $lower;
echo '<pre>';

//do while
$i = 0;
$lower = '';
do {
    $lower .= mb_strtolower($str[$i]);
    $i++;
}
while (isset($str[$i]));
echo $lower;
echo '<pre>';

//foreach
$i = 0;
$arr = [];
$lower = '';
while (isset($str[$i])) {
    $arr[] = mb_strtolower($str[$i]);
    $i++;
}
$i = 0;
foreach ($arr as $value) {
    $lower .= $arr[$i];
    $i++;
}
echo $lower;
echo '<pre>';


//5***
//for
echo '<pre>';
$str = 'Hi I am ALex';
for($i=0; $i<12; $i++){
    echo mb_strtoupper($str[$i]).' ';
}
echo '<pre>';

//while
$i = 0;
$lower = '';
while (isset($str[$i])) {
    $lower .= mb_strtoupper($str[$i]);
    $i++;
}
echo $lower;
echo '<pre>';

//do while
$i = 0;
$lower = '';
do {
    $lower .= mb_strtoupper($str[$i]);
    $i++;
}
while (isset($str[$i]));
echo $lower;
echo '<pre>';

//foreach
$i = 0;
$arr = [];
$lower = '';
while (isset($str[$i])) {
    $arr[] = mb_strtoupper($str[$i]);
    $i++;
}
$i = 0;
foreach ($arr as $value) {
    $lower .= $arr[$i];
    $i++;
}
echo $lower;
echo '<pre>';

//7***
//for
echo '<pre>';
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
for($i=0; $i<5; $i++){
    print_r( mb_strtolower($arr[$i]).' ');
}
echo '<pre>';

//while
$i = 0;
$lower = '';
while (isset($arr[$i])) {
    $lower .= mb_strtolower($arr[$i].' ');
    $i++;
}
echo $lower;
echo '<pre>';

//do while
$i = 0;
$lower = '';
do {
    $lower .= mb_strtolower($arr[$i].' ');
    $i++;
}
while (isset($arr[$i]));
echo $lower;
echo '<pre>';

//foreach
$i = 0;
$array = [];
$lower = '';
while (isset($arr[$i])) {
    $array[] = mb_strtolower($arr[$i].' ');
    $i++;
}
$i = 0;
foreach ($array as $value) {
    $lower .= $array[$i];
    $i++;
}
echo $lower;
echo '<pre>';

//8***
//for
echo '<pre>';
for($i=0; $i<5; $i++){
    print_r( mb_strtoupper($arr[$i]).' ');
}
echo '<pre>';
echo '<pre>';

//while
$i = 0;
$lower = '';
while (isset($arr[$i])) {
    $lower .= mb_strtoupper($arr[$i].' ');
    $i++;
}
echo $lower;
echo '<pre>';

//do while
$i = 0;
$lower = '';
do {
    $lower .= mb_strtoupper($arr[$i].' ');
    $i++;
}
while (isset($arr[$i]));
echo $lower;
echo '<pre>';

//foreach
$i = 0;
$array = [];
$lower = '';
while (isset($arr[$i])) {
    $array[] = mb_strtoupper($arr[$i].' ');
    $i++;
}
$i = 0;
foreach ($array as $value) {
    $lower .= $array[$i];
    $i++;
}
echo $lower;
echo '<pre>';

//9***
//for
$num='12345678';
for($i=7; $i>=0; $i--){
    echo $num[$i].' ';
}
echo '<pre>';

//while
$num = (string)$num;
$i = 0;
$index = 0;
$reverse = '';
while (isset($num[$i])) {
    $index = $i;
    $i++;
}
while ($index >= 0) {
    $reverse .= $num[$index];
    $index--;
}
$reverse = (integer)$reverse;
echo $reverse;
echo '</pre>';

//do while
$num = (string)$num;
$i = 0;
$index = 0;
$reverse = '';
while (isset($num[$i])) {
    $index = $i;
    $i++;
}
do {
    $reverse .= $num[$index];
    $index--;
}
while ($index >= 0);
$reverse = (integer)$reverse;
echo $reverse;
echo '</pre>';

//foreach
$num = (string)$num;
$i = 0;
$index = 0;
$arr = [];
$reverseArr = [];
$reverse = '';
while (isset($num[$i])) {
    $arr[] = $num[$i];
    $i++;
}
foreach ($arr as $key => $value) {
    if (isset($arr[$key])) {
        $i = $key;
    }
}
foreach ($arr as $key => $value) {
    $reverseArr[] = $arr[$i];
    $index = $i;
    $i--;
}
foreach ($reverseArr as $value) {
    $reverse .= $reverseArr[$index];
    $index++;
}
$reverse = (integer)$reverse;
echo $reverse;
echo '</pre>';

//10***
//for
$arr= [44, 12, 11, 7, 1, 99, 43, 5, 69];
for ($i=0;$i<9;$i++){
    for($j=0;$j<9;$j++) {
        if ($arr[$j] < $arr[$j + 1]) {
            $n = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $n;
        }
    }
}
print_r($arr);
echo '</pre>';

//while
$count = 0;
$i = 0;
while (isset($arr[$i])){
    $count = $i + 1;
    $i++;
}
$i = 0;
$j = 0;
while ($i < $count){
    while ($j < $count-1){
        if($arr[$j] < $arr[$j+1]) {
            $num = $arr[$j+1];
            $arr[$j+1]=$arr[$j];
            $arr[$j]=$num;
        }
        $j++;
    }
    $i++;
    if ($j == $count-1)
        $j = 0;
}
print_r($arr);
echo '<pre>';

//do while
$count = 0;
$i = 0;
do {
    $count = $i;
    $i++;
} while (isset($arr[$i]));
$i = 0;
$j = 0;
do {
    do {
        if ($arr[$j] < $arr[$j + 1]) {
            $num = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $num;
        }
        $j++;
    } while ($j > $count);
    $i++;
    if ($j == $count)
        $j = 0;
} while ($i > $count);
print_r($arr);
echo '<pre>';

//foreach
$count = 0;
for ($i = 0; isset($arr[$i]); $i++)
    $count = $i;
foreach ($arr as $value) {
    for ($j = 0; $j < $count; $j++) {
        if ($arr[$j] < $arr[$j + 1]) {
            $num = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $num;
        }
    }
}
print_r($arr);
echo '<pre>';
?>