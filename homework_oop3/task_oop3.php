<?php


interface Live{
    function live();
}

abstract class LiveIn implements Live {
    function live()
    {
        echo 'I live in ' . strtolower(get_class($this));
        echo '<br>' . $this->placeInfo(). ' (' . $this->placeType . ')' . '<br>';
    }
    abstract protected function placeInfo();
}

class AtLarge extends LiveIn {
    protected function placeInfo()
    {
        return "atLarge Info";
    }
}
class Home  extends LiveIn {
    protected $placeType;

    public function __construct($placeType){
        $this->placeType=$placeType;
    }

    protected function placeInfo()
    {
        return "home Info";
    }
}


class Mlekopit {

    protected $kind;

    public function getInfo()
    {
        return ('<br>' . $this->kind);
    }
}

class Animals extends Mlekopit {

    protected $levelOfEvolution, $kingdom;

    public function __get($a)
    {
        if(property_exists($this,$a)){
            return $this->$a;
        }
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>Level of evolution: ' . $this->levelOfEvolution . ' , Animal kingdom: ' . $this->kingdom;
    }
}

class  Humanoobr extends Animals {

    protected $name, $rasa, $timeOfLife;

    public function __get($a)
    {
        if (property_exists($this, $a)) {
            return $this->$a;
        }
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>' . $this->name .
            ' , ' . $this->rasa .
            ' , ' . $this->timeOfLife;
    }

}
class  Human extends Humanoobr {

    protected $initials, $age, $gender;
    private $live;

    public function __construct($kind, $levelOfEvolution, $kingdom,
                                $name, $rasa, $timeOfLife, $initials,
                                $age, $gender )
    {
        $this->kind = $kind;
        $this->levelOfEvolution = $levelOfEvolution;
        $this-> kingdom= $kingdom;
        $this-> name= $name;
        $this-> rasa= $rasa;
        $this-> timeOfLife= $timeOfLife;
        $this-> initials= $initials;
        $this-> age= $age;
        $this-> gender= $gender;
    }

    public function __get($a)
    {
        if (property_exists($this, $a)) {
            return $this->$a;
        }
    }

    public function __set($b, $c)
    {
        if (property_exists($this, $b)) {
            $this->$b = $c;
        }
    }

    public function selectPlace($place)
    {
        !is_object($place) ? : $this->live = $place;
    }
    public function live()
    {
        method_exists($this->live, 'live') &&
        is_subclass_of($this->live, "LiveIn") ?
            $this->live->live() : "I don't live";
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>' . $this->initials .
            ' , ' . $this->age .
            ' , ' . $this->gender;
    }

}

$flat = new Home('Flat');
$ann= new Human('Placental mammals','Primates' ,
    'high', 'human', 'Hominids', '3761 г. до н. э.',
    'A.S.O.', '56', 'female');

echo  $ann->getInfo() . '<hr>';

$ann->selectPlace($flat);
$ann->live();

echo '<hr>Human initial: ' . $ann->get_initials;
echo '<br>Age: ' . $ann->age;

$ann->gender = 'unknown';

echo '<br>Ann\'s new gender: ' . $ann->gender;


